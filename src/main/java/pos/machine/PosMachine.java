package pos.machine;

import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class PosMachine {
    static int total = 0;

    public String printReceipt(List<String> barcodes) {
        if (isListNull(barcodes)) return null;
        //Count the quantity of each product, TreeMap is needed here to traverse in label order
        TreeMap<String, Integer> countMap = countNumber(barcodes);

        //Encapsulate each product information into a Map, using barcode as the key
        HashMap<String, Item> goodsMap = getGoodsMap();

        //Generate the entire Receipt
        String receipt = generateAll(countMap, goodsMap);
        return receipt;
    }

    private static String generateAll(TreeMap<String, Integer> countMap, HashMap<String, Item> goodsMap) {
        StringBuilder receipt = new StringBuilder("***<store earning no money>Receipt***\n");
        for (String barcode : countMap.keySet()) {
            System.out.println("barcode:" + barcode);
            if (isExist(barcode, goodsMap)) {
                // Generate a list of individual product information and add it to it
                receipt.append(generateOne(goodsMap, barcode, countMap));
            }
        }
        receipt.append("----------------------\n" + "Total: ").append(total)
                .append(" (yuan)\n" + "**********************");
        return receipt.toString();
    }

    private static String generateOne(HashMap<String, Item> goodsMap, String barcode, TreeMap<String, Integer> countMap) {
        StringBuilder singleReceipt = new StringBuilder();
        String name = goodsMap.get(barcode).getName();
        int number = countMap.get(barcode);
        int price = goodsMap.get(barcode).getPrice();
        int subtotal = getSubtotal(number, price);
        total += subtotal;
        singleReceipt.append("Name: ").append(name);
        singleReceipt.append(", Quantity: ").append(number);
        singleReceipt.append(", Unit price: ").append(price);
        singleReceipt.append(" (yuan), Subtotal: ").append(subtotal).append(" (yuan)\n");
        return singleReceipt.toString();
    }

    private static int getSubtotal(int number, int price) {
        return number * price;
    }

    private static boolean isExist(String barcode, HashMap<String, Item> goodsMap) {
        return goodsMap.containsKey(barcode);
    }

    private static TreeMap<String, Integer> countNumber(List<String> barcodes) {
        TreeMap<String, Integer> countMap = new TreeMap<>();
        for (String barcode : barcodes) {
            if (!countMap.containsKey(barcode)) {
                countMap.put(barcode, 1);
            } else {
                countMap.put(barcode, countMap.get(barcode) + 1);
            }
        }
        return countMap;
    }

    private static boolean isListNull(List<String> barcodes) {
        if (barcodes == null || barcodes.size() == 0) {
            return true;
        }
        return false;
    }

    private static HashMap<String, Item> getGoodsMap() {
        List<Item> items = ItemsLoader.loadAllItems();
        HashMap<String, Item> goodsMap = new HashMap<>();
        for (Item item : items) {
            goodsMap.put(item.getBarcode(), item);
        }
        return goodsMap;
    }
}
