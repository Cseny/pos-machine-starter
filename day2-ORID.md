# O

- The significance of tasking for work.

- How to develop a tasking..

- How to use Git.

- Practice drawing context maps.

# R

This is a fruitful day.

# I

Learned how to use the tasking method to split problems and draw context maps based on tasking. Reviewed the various commands of Git.

# D

I will try more to use tasking to break down and solve problems in my future studies and work.